﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Alarm : MonoBehaviour
{
    [SerializeField] private UnityEvent _doorIsOpen;
    [SerializeField] private UnityEvent _badGuyLeftHouse;
    [SerializeField] private Transform _dynamicVolumeValue;
    [SerializeField] private float _volume;

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.TryGetComponent(out Movement badGuyRunningSpeed))
        {            
            badGuyRunningSpeed.speed = 7f;
            _doorIsOpen?.Invoke();
        }

        if (collision.TryGetComponent(out Transform badGuyCoordinates))
        {            
            if (badGuyCoordinates.position.x<transform.position.x)
            {                
                _badGuyLeftHouse?.Invoke();
            }
        }

    }

    void Update()
    {
        AudioListener.volume = (1+_dynamicVolumeValue.position.x) * Time.deltaTime * _volume;
    }
}
