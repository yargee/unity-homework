﻿using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.Events;

public class Movement : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _rigibody;    
    public float speed;

    void Update()
    {
        _rigibody.transform.Translate(speed*Time.deltaTime, 0, 0);                
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            transform.Rotate(0, 180, 0);
        }
    }
}
